export GRADLE_HOME={{ __gradle_install_dir }}/gradle-{{ gradle_version }}
export PATH=$PATH:$GRADLE_HOME/bin
